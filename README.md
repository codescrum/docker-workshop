# Docker workshop

## Warm-up the images:

```
docker pull alpine:3.3
docker pull nginx:1.8-alpine
docker pull redis:alpine
docker pull mhart/alpine-node:latest
```

## Docker run example

```
cd code/hello-world
docker run -P nginx:1.8-alpine
docker run --name hello --volume="$(pwd)/site:/usr/share/nginx/html" -p 8080:80 nginx:1.8-alpine
docker logs hello
docker rm -f hello
```

## Docker build example

```
cd code/docker-apache2
docker build -t docker-apache2 .
docker rm -f hello
docker run -d --name hello -p 4003:80 docker-apache2
docker rm -f hello
docker run -d --name hello -p 4003:80 --volume="$(pwd)/site:/var/www/localhost/htdocs" docker-apache2
docker stop hello
docker rm -f hello
```

## Docker compose example

```
cd code/guestbook-node
docker-compose build
docker-compose up
docker-compose rm -af
docker-compose up
```
